﻿using System;
using UnityEngine;

/**
 * Utility class with functions that are common to many game entities.
 */
class Util {

    /**
     * Find an object with provided tag in the direct visibility from the provided position.
     */
    public static Transform findNewTarget(Vector3 position, String enemyTag) {
        Transform target = null;

        GameObject[] enemies;
        enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        GameObject closest = null;
        float distance = Mathf.Infinity;
        foreach (GameObject enemy in enemies)
        {
            //Debug.Log(enemy.name + " is a potential target");
            //Debug.DrawLine(position, enemy.transform.position, Color.magenta);

            // check if there are other objects between them
            RaycastHit hit;
            //Debug.DrawRay(position, enemy.transform.position, Color.yellow);
            //Debug.Break();

            if (Physics.Raycast(position, enemy.transform.position - position, out hit))
            {
                //Debug.Log(hit.collider.name + " between player and a potential target");
                if (hit.collider.gameObject == enemy)
                {
                    Vector3 diff = enemy.transform.position - position;
                    float curDistance = diff.sqrMagnitude;
                    if (curDistance < distance)
                    {
                        closest = enemy;
                        distance = curDistance;
                    }
                }
            }

        }

        if (closest != null)
        {
            target = closest.transform;
        }

        return target;
    }

    /**
     * Check if there is a clear way between two points in the scene. 
     * The way is considered clear, if there are no walls in the way.    
     */
     // TODO there is another implementation of this function somewhere
    public static bool isWayClear(Vector3 from, Vector3 to)
    {

        RaycastHit[] collisions = Physics.RaycastAll(from, to - from, Vector3.Distance(from, to));

        foreach (RaycastHit collision in collisions)
        {
            string colliderName = collision.collider.gameObject.name;
            if (colliderName.Contains("Wall"))
            {
                return false;
            }
        }
        return true;
    }

}