﻿using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * A manager singleton, which handles the level switching. It is meant to transfer relevant data between the scenes.
 */
internal class SceneManager
{
    private static SceneManager instance;

    private SceneManager()
    {
        return;
    }

    public static SceneManager getInstance()
    {
        if (instance == null)
        {
            instance = new SceneManager();
        }
        return instance;
    }

    /**
     * Change the scene, after the player won
     */
    internal void playerWonShooting(LinkedList<string> activePlayers, LinkedList<string> killedPlayers, int numberOfKilledEnemies)
    {
        Application.LoadLevel("MacroGame");
    }
}