﻿using UnityEngine;
using System.Collections;

/**
 * Marker that follows the mouse.
 */
public class MouseMarker : MonoBehaviour
{

    public Transform ground;

    // Update is called once per frame
    void Update()
    {
        // place the marker, where the mouse hits the ground plane
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Plane plane = new Plane(ground.up, Vector3.zero);
        float hitDistance;
        if (plane.Raycast(ray, out hitDistance))
        {
            Vector3 mousePos = ray.GetPoint(hitDistance);
            transform.position = mousePos;
        }
    }
}
