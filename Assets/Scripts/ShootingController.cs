﻿using UnityEngine;
using System.Collections;
using System;

/**
* Player and enemy figure controller.
* If you see an enemy - aim and shoot at it.
*/
public class ShootingController : MonoBehaviour
{

    internal float range;
    public String enemyName = "Enemy";
    public GameObject bullet;
    public Transform barrel;
    private Vector3 missMarker = new Vector3(-1000, -1000, -1000);

    private float aimTime = 0;
    private const float MAX_AIM_TIME = 1f;

    void Start()
    {
        BulletController bc = bullet.GetComponent<BulletController>();
        range = bc.range;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = getTargetInAim();
        if (target != missMarker)
        {
            if (isAimDone())
            {
                shoot(target);
            }
        }
        else
        {
            // aim broken
            aimTime = 0;
        }
    }

    /**
     * Check if the time, that was neede to aim, has passed.
     * This function simulates cooldown of the gun.
     */
    private bool isAimDone()
    {
        aimTime += Time.deltaTime;
        if (aimTime >= MAX_AIM_TIME)
        {
            aimTime = 0f;
            return true;
        }
        return false;
    }

    /**
     * Spit a bullet at the target.
     */
    private void shoot(Vector3 target)
    {
        GameObject bulletClone = (GameObject)Instantiate(bullet, barrel.position, barrel.rotation);
        BulletController bulletController = bulletClone.GetComponent<BulletController>();
        bulletController.target = target;
        bulletController.recalculateTarget();

        if (tag.Equals("player"))
        {
            bulletClone.GetComponent<Material>().color = Color.blue;
        }
    }

    /**
     * Check if looking at a valid target. If yes - return its coordinates.
     */
    private Vector3 getTargetInAim()
    {
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.forward, range);

        foreach (RaycastHit hit in hits)
        {
            String name = hit.collider.gameObject.tag;

            if (enemyName.Equals(name))
            {
                return hit.point;
            }

        }

        return missMarker;
    }
}
