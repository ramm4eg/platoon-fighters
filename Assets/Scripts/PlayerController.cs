﻿using UnityEngine;

/**
 * Controller for player figures.
 * 
 * It actually has two controllers in it - it evaluates the position of the figure in the platoon.
 * If the figure is in the first position it is concidered a leader and it moves towards the mouse marker.
 * If there is another figure infront of this one, it moves to stay behind the figure in front.
 *
 * The behaviour updates automatically, when other figures in the platoon get destroyed.
 *
 * In any case the figure looks either in the direction of a nearby enemy or in the direction of its movement. 
 */
public class PlayerController : MonoBehaviour
{

    private float speed = 75.0f;
    private float rotationSpeed = 360.0f;

    public Transform[] platoon;
    private bool iAmLeader = false;

    public Transform markerObject;

    private Vector3 movingPos;
    private Transform whomToFollow;
    public float distanceBehindFollowee = 10f;

    private Transform target;

    private int hp = 5;
    public string playerName;

    // called in every frame
    public void Update()
    {
        // Movement
        if (Input.GetMouseButton(0))
        {
            if (!iAmLeader && whomToFollow == null)
            {
                findWhomToFollow();
            }

            // am I a leader or a follower?
            if (iAmLeader)
            {
                // lead platoon to the marker
                movingPos = markerObject.position;
            }
            else
            {
                // follow the leader
                Vector3 direction = whomToFollow.position - markerObject.position;
                Ray ray = new Ray(markerObject.position, direction);
                float distance = Vector3.Distance(whomToFollow.position, markerObject.position);
                distance += distanceBehindFollowee;
                movingPos = ray.GetPoint(distance);
            }
            movingPos.y = 5;
            transform.position = Vector3.MoveTowards(transform.position, movingPos, speed * Time.deltaTime);
        }

        // Rotation
        if (target != null && target.gameObject.activeInHierarchy && isInRange(target))
        {
            Quaternion q = Quaternion.LookRotation(target.position - transform.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, rotationSpeed * Time.deltaTime);
        }
        else
        {
            Vector3 position = transform.position;
            target = Util.findNewTarget(position, "Enemy");

            // if I have no target - I will look the direction I move in
            Quaternion q = Quaternion.LookRotation(movingPos - transform.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, rotationSpeed * Time.deltaTime);
        }
    }

    /**
     * Check if the target is in shot range.
     */
    private bool isInRange(Transform target)
    {
        float distance = Vector3.Distance(transform.position, target.position);
        float range = GetComponent<ShootingController>().range;
        return distance <= range;
    }

    /**
     * Go through platoon and find the figure that is in front of this one. 
     */
    private void findWhomToFollow()
    {
        Transform previous = null;
        Transform current = platoon[0];
        for (int i = 1; i < platoon.Length && current != transform; i++)
        {
            if (platoon[i - 1] != null)
            {
                previous = platoon[i - 1];
            }
            current = platoon[i];
        }

        if (previous == null)
        {
            iAmLeader = true;
        }
        else
        {
            whomToFollow = previous;
        }
    }

    /**
     * Calculate a collision.
     * If hit by a bullet reduce HP and eventually die.
     */
    void OnCollisionEnter(Collision collision)
    {
        GameObject hitter = collision.collider.gameObject;
        if (hitter.tag.Equals("EnemyBullet"))
        {
            hp--;
            if (hp <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
