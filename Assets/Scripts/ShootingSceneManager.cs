﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/**
 * Manager of the shooting scene.
 * Gathers the information about the scene. Triggers the end, 
 * when player has lost all his figures or killed all enemies.
 * This script is meant to provide the relevant data to SceneManager,
 * so it can be transfered to other scenes.
 */
public class ShootingSceneManager : MonoBehaviour {
    private ShootingSceneManager instance = null;

    private int activeEnemiesCount;
    private int overallEnemiesCount;
    private LinkedList<string> activePlayers;
    private LinkedList<string> killedPlayers;
    private bool guiActive = false;


    // Use this for initialization
    void Start () {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            GameObject.Destroy(this);
            return;
        }

        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        activeEnemiesCount = enemies.Length;
        overallEnemiesCount = enemies.Length;

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        activePlayers = new LinkedList<string>();
        foreach (GameObject player in players)
        {
            PlayerController contr = player.GetComponent<PlayerController>();
            activePlayers.AddLast(contr.playerName);
        }

        killedPlayers = new LinkedList<string>();

        Node.instantiate();
    }

    void OnGUI()
    {
        if (!guiActive && activeEnemiesCount == 0)
        {
            Time.timeScale = 0;

            guiActive = true;

            GameObject canvas = GameObject.FindGameObjectWithTag("GUI");
            Component[] rects = canvas.GetComponentsInChildren(typeof(RectTransform), true);
            foreach (Component rect in rects)
            {
                if ("YouWinPanel".Equals(rect.gameObject.name))
                {
                    rect.gameObject.SetActive(true);
                }
            }

        }
    }

    /**
     * Called by enemies when they die.
     */
    public void enemyDied()
    {
        activeEnemiesCount--;
    }

    /**
     * Called by player figures when they die.
     */
    public void playerDied(string playerName)
    {
        activePlayers.Remove(playerName);
        killedPlayers.AddLast(playerName);
    }

    /**
     * Gather the data from the scene and trigger the scene manager.
     */
    public void reportSceneResults()
    {

        int numberOfKilledEnemies = overallEnemiesCount - activeEnemiesCount;

        SceneManager sceneManager = SceneManager.getInstance();
        sceneManager.playerWonShooting(activePlayers, killedPlayers, numberOfKilledEnemies);
    }

    /**
     * Get player figures, that survived until now.
     */
    private string[] getLivingPlayers()
    {
        throw new NotImplementedException();
    }
}
