﻿using System;
using UnityEngine;

/**
 * Enemy controller.
 * Enemies controlled by this script don't move. They just turn towards player figures.
 */
public class StaticEnemyController : AbstractEnemyController
{
    protected override void moveTowards(Transform target)
    {
        // do nothing
    }
}
