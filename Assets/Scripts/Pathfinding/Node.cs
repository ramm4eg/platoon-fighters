﻿using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * A node in the pathfinding algorithm.
 */
internal class Node
{
    private static float GRANULARITY = 10.0f;
    private static Dictionary<int, Dictionary<int, Node>> nodes;

    private int x;
    private int z;
    private Vector3 vector;
    private LinkedList<Node> neighbors = null;

    /**
     * Constructor.
     * Create node with provided coordinates.
     */
    public Node(int intX, int intZ)
    {
        x = intX;
        z = intZ;
        vector = new Vector3(intX * GRANULARITY, 5, intZ * GRANULARITY);
        
        //Debug.Log("Creating node [" + x + "][" + z + "]");
        //Debug.Log("According vector (" + vector.x + "," + vector.y + "," + vector.z + ")");
    }

    /**
     * Get a node for a provided vector.
     */
    internal static Node get(Vector3 vector)
    {
        int x = Convert.ToInt32(Math.Round(vector.x));
        int z = Convert.ToInt32(Math.Round(vector.z));

        int decX = RoundOff(x);
        int decZ = RoundOff(z);

        Node result = get(decX, decZ);
        return result;
    }

    /**
     * Get a node for provided coordinates. If it doesn't exist yet - create it.
     */
    private static Node get(int x, int z)
    {

        Dictionary<int, Node> nodesForX;
        if (!nodes.TryGetValue(x, out nodesForX))
        {
            nodesForX = new Dictionary<int, Node>();
            nodes.Add(x, nodesForX);
        }

        Node result;
        if (!nodesForX.TryGetValue(z, out result))
        {
            result = new Node(x, z);
            nodesForX[z] = result;
        }

        return result;
    }

    /**
     * Create nodes for the scene. Start with (0, 0) and instatiate all it's neighbors. 
     * And then all the neighbors of the neighbors. And so on, until the walls are reached.
     */
    internal static void instantiate()
    {
        nodes = new Dictionary<int, Dictionary<int, Node>>();

        LinkedList<Node> newNodes = new LinkedList<Node>();
        LinkedList<Node> managedNodes = new LinkedList<Node>();

        Node start = get(0, 0);
        newNodes.AddFirst(start);

        while (newNodes.Count >0)
        {
            Node current = newNodes.Last.Value;
            newNodes.RemoveLast();
            managedNodes.AddLast(current);

            LinkedList<Node> neighbors = current.getValidNeighbors();
            foreach (Node neighbor in neighbors)
            {
                if (!managedNodes.Contains(neighbor))
                {
                    newNodes.AddLast(neighbor);
                }
            }
        }
    }

    /**
     * Round off an integer to the precision of GRANULARITY.
     */
    private static int RoundOff(int i)
    {
        return (int)Math.Round(i / GRANULARITY);
    }

    /**
     * Get position in the world, which corresponds with this node.
     * For node (x, z) the corresponding vector is (x * GRANULARITY, 5, z * GRANULARITY)
     */
    internal Vector3 getVector()
    {
        return vector;
    }

    /**
     * Get neighbors of the node.
     * The neighbor node is concidered valid (and therefore existing), if there is no wall in the radius of half GRANULARITY around the node.
     */
    internal LinkedList<Node> getValidNeighbors()
    {
        if (neighbors == null)
        {
            Node[] potentials = new Node[8];
            potentials[0] = get(x + 1, z);
            potentials[1] = get(x - 1, z);
            potentials[2] = get(x + 1, z + 1);
            potentials[3] = get(x, z + 1);
            potentials[4] = get(x - 1, z + 1);
            potentials[5] = get(x + 1, z - 1);
            potentials[6] = get(x, z - 1);
            potentials[7] = get(x - 1, z - 1);

            neighbors = new LinkedList<Node>();

            // check neighbors, if they are not too close to walls
            foreach (Node neighbor in potentials)
            {
                if (!neighbor.tooCloseToAWall())
                {
                    neighbors.AddLast(neighbor);
                }
            }
        }


        return neighbors;
    }

    /**
     * Check if the node is too close to a wall to be concidered valid.
     * The node is concidered valid (and therefore existing), if there is no wall in the radius of half GRANULARITY around the node.
     */
    private bool tooCloseToAWall()
    {
        Collider[] colliders = Physics.OverlapSphere(vector, GRANULARITY / 2.0f);
        if (colliders.Length == 0)
        {
            return false;
        }

        foreach (Collider col in colliders)
        {
            if (col.gameObject.name.Contains("Wall"))
            {
                return true;
            }
        }
        return false;
    }
}