﻿using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * Implementation of A* algorithm, that is bound to one path seeking object in the scene.
 */
internal class Pathfinding
{
    /**
     * States of the path search.
     */
    internal enum State
    {
        RESET, SEARCHING, DONE
    }

    internal volatile State State_;

    private Vector3 start;
    private Vector3 target;
    private Node startNode;
    private Node targetNode;
    private Node current;
    private Dictionary<Node, Node> path;
    private LinkedList<Node> targetNeighbors;

    /*
     * Constructor.
     */
    internal Pathfinding()
    {
        path = new Dictionary<Node, Node>();
        reset();
    }

    /**
     * Set all fields to their initial state.
     */
    internal void reset()
    {
        State_ = State.RESET;
        start = Vector3.zero;
        target = Vector3.zero;
        startNode = null;
        current = null;
        targetNode = null;
        targetNeighbors = null;
        path.Clear();
    }

    /**
     * Search for the path.
     */
    internal void findPath()
    {
        State_ = State.SEARCHING;
        // Debug.Log("findPath");
        LinkedList<Node> done = new LinkedList<Node>();
        LinkedList<Node> todo = new LinkedList<Node>();

        Dictionary<Node, float> distanceFromStartToNode = new Dictionary<Node, float>();
        Dictionary<Node, float> distanceFromStartToGoalViaNode = new Dictionary<Node, float>();

        startNode = Node.get(start);
        
        todo.AddLast(startNode);
        distanceFromStartToNode[startNode] = 0f;
        distanceFromStartToGoalViaNode[startNode] = distanceFromStartToNode[startNode] + calculateDistance(startNode, targetNode);
        
        while (todo.Count > 0)
        {
            // Debug.Log("Iterating...");
            if (State_ != State.SEARCHING)
            {
                // canceled
                return;
            }
            
            current = getTheLowestDistanceNode(todo, distanceFromStartToGoalViaNode);
            
            if (isNearTarget(current))
            {
                State_ = State.DONE;
                // Debug.Log("Path found");
                return;
            }

            todo.Remove(current);
            done.AddFirst(current);

            LinkedList<Node> neighbors = current.getValidNeighbors();

            foreach (Node node in neighbors)
            {
                if (!done.Contains(node))
                {
                    float distanceFromStart = distanceFromStartToNode[current] + calculateDistance(current, node);
                    if (!todo.Contains(node))
                    {
                        todo.AddFirst(node);
                    }
                    else if (distanceFromStart >= distanceFromStartToNode[node])
                    {
                        // this is not the best way
                        continue;
                    }

                    // this is potentially the optimal path
                    path[node] = current;
                    distanceFromStartToNode[node] = distanceFromStart;
                    distanceFromStartToGoalViaNode[node] = distanceFromStart + calculateDistance(node, targetNode);
                }
            }
        }
    }

    /**
     * Check if the target node is reached in the found path.
     */
    private bool isNearTarget(Node current)
    {
        return targetNeighbors.Contains(current);
    }

    /**
     * Set the target to find the path to.
     */
    internal void setTarget(Transform transform)
    {
        this.target = transform.position;
        targetNode = Node.get(target);
        targetNeighbors = targetNode.getValidNeighbors();
    }

    /**
     * Set the start position to search the path from.
     */
    internal void setStart(Vector3 position)
    {
        this.start = position;
    }

    /**
     * Calculate the distance between two nodes.
     * This method is used to calculate the distance between two neighboring nodes and 
     * to calculate the distance from a node to the target node, which is the heuristical
     * approximation of the path to go.
     */
    private float calculateDistance(Node start, Node goal)
    {
        //Debug.Log("calculateDistance");
        return Vector3.Distance(start.getVector(), goal.getVector());
    }

    /**
     * Get the node, which is the most promissing to be the next step in the path.
     */
    private Node getTheLowestDistanceNode(LinkedList<Node> todo, Dictionary<Node, float> distanceFromStartToGoalViaNode)
    {
        // Debug.Log("getTheLowestDistanceNode");
        float minDistance = float.PositiveInfinity;
        Node result = null;
        foreach (Node node in todo)
        {
            float distance = distanceFromStartToGoalViaNode[node];
            if (distance < minDistance)
            {
                minDistance = distance;
                result = node;
            }
        }
        return result;
    }

    /**
     * Find the position to move to, which is the furthest visible node from the found path.
     */
    internal Vector3 getMoveDestination()
    {
        // Debug.Log("getMoveDestination");
        while (current != startNode)
        {
            if (Util.isWayClear(startNode.getVector(), current.getVector()))
            {
                Debug.DrawRay(start, current.getVector() - start, Color.cyan);
                return current.getVector();
            }
            current = path[current];
        }
        // if no node is reachable - stand where you are (can actually never happen)
        return startNode.getVector();
    }
}