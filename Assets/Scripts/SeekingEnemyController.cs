﻿using UnityEngine;
using System.Threading;

/**
 * Enemy controller.
 * Enemies controlled by this script seek the path to the player and follow it.
 */
public class SeekingEnemyController : AbstractEnemyController
{
    private float speed = 30f;

    private Vector3 movingPos;
    private Vector3 lastSeenPos = Vector3.zero;
    Pathfinding pathfinding;

    void Start()
    {
        pathfinding = new Pathfinding();
    }
    
    /**
     * Move towards target.
     * If the target is visible - move towards it until it's in shot range.
     * If the target is not visible - move towards the position, where it was seen last. In the meantime calculate the path to the enemy.
     * When the path is found - follow it.
     */
    protected override void moveTowards(Transform target)
    {
        if (Util.isWayClear(transform.position, target.position))
        {
            pathfinding.reset();
            lastSeenPos = target.position;
            if (Vector3.Distance(transform.position, target.position) < range)
            {
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                return;
            }
            movingPos = target.position;
        }
        else
        {
            Thread workerThread = null;
            switch (pathfinding.State_)
            {
                case Pathfinding.State.RESET:
                    // start new search
                    // Debug.Log("Starting pathfinding");
                    pathfinding.setStart(transform.position);
                    pathfinding.setTarget(target);
                    workerThread = new Thread(pathfinding.findPath);
                    workerThread.Start();
                    movingPos = lastSeenPos;
                    break;
                case Pathfinding.State.SEARCHING:
                    // wait for search to end
                    // Debug.Log("Still searching...");
                    movingPos = lastSeenPos;
                    break;
                case Pathfinding.State.DONE:
                    // path found - move to next waypoint
                    // Debug.Log("Path found");
                    if (workerThread != null)
                    {
                        workerThread.Join();
                    }
                    movingPos = pathfinding.getMoveDestination();
                    break;

            }
        }

        // do the moving
        movingPos.y = 5;
        Debug.DrawRay(transform.position, movingPos - transform.position, Color.yellow);
        if (Util.isWayClear(transform.position, movingPos) && !movingPositionReached())
        {
            //transform.position = Vector3.MoveTowards(transform.position, movingPos, speed * Time.deltaTime);
            var heading = movingPos - transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;
            GetComponent<Rigidbody>().velocity = direction * speed;
        }
        else
        {
            // can't move further - either forget the target or wait to find the path
            if (pathfinding.State_ != Pathfinding.State.SEARCHING)
            {
                pathfinding.reset();
                target = null;
            }
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    /**
     * Check if the position we are moving towards is reached.
     */
    private bool movingPositionReached()
    {
        return Vector3.Distance(transform.position, movingPos) <= 1;
    }

    /**
     * Check if the target is visible from provided point in scene.
     */
    private bool isTargetVisible(Vector3 from, Transform target)
    {
        RaycastHit hit;
        if (Physics.Raycast(from, target.position - from, out hit))
        {
            if (hit.collider.transform == target)
            {
                return true;
            }
        }
        return false;
    }
    
}
