﻿using UnityEngine;

/**
 * Enemy controller.
 * Enemies controlled by this script move to the point, where they have last seen the player.
 */
public class FollowingEnemyController : AbstractEnemyController
{
    private float speed = 30f;

    private Vector3 movingPos;
    private Vector3 lastSeenPos = Vector3.zero;

    /**
     * Move towards target.
     * If the target is visible - move towards it until it's in shot range.
     * If the target is not visible - move towards the position, where it was seen last. If the position is reached and target still can not be seen - forget it.
     */
    override protected void moveTowards(Transform target)
    {
        if (Util.isWayClear(transform.position, target.position))
        {
            lastSeenPos = target.position;
            if (Vector3.Distance(transform.position, target.position) < range)
            {
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                return;
            }
            movingPos = target.position;
        }
        else
        {
            if (lastSeenPos.Equals(Vector3.zero))
            {
                return;
            }
            movingPos = lastSeenPos;
        }
        movingPos.y = 5;

        if (Util.isWayClear(transform.position, movingPos) && !movingPositionReached())
        {
            var heading = movingPos - transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;
            GetComponent<Rigidbody>().velocity = direction * speed;
        }
        else
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            target = null;
        }
    }

    /**
     * Check if the position we are moving towards is reached.
     */
    private bool movingPositionReached()
    {
        return Vector3.Distance(transform.position, movingPos) <= 1;
    }

    
}
