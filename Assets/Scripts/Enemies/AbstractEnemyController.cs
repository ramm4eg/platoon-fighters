﻿using System;
using UnityEngine;

/**
 * Enemy controller.
 */
public abstract class AbstractEnemyController : MonoBehaviour
{

    public Transform target;
    public float rotationSpeed = 200.0f;
    private int hp = 5;
    public float range = 100f;

    // Update is called once per frame
    void Update()
    {
        if (target != null && target.gameObject.activeInHierarchy)
        {
            // look at the player's direction
            Quaternion q = Quaternion.LookRotation(target.position - transform.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, rotationSpeed * Time.deltaTime);

            // move to player
            moveTowards(target);
        }
        else
        {
            Vector3 position = transform.position;
            target = Util.findNewTarget(position, "Player");
        }
    }

    /**
     * Move towards target.
     */
    protected abstract void moveTowards(Transform target);

    /**
     * Calculate collision.
     * If the other collider is a bullet - reduce HP. If all HP lost - destroy the object.
     */
    void OnCollisionEnter(Collision collision)
    {
        GameObject hitter = collision.collider.gameObject;
        if (hitter.tag.Equals("PlayerBullet"))
        {
            hp--;
            if (hp <= 0)
            {
                Destroy(gameObject);
            }
        }
    }

    /**
     * Upon death notify the scene manager. When no enemies left in the scene - level is done.
     */
    void OnDisable()
    {
        GameObject sceneManager = GameObject.FindGameObjectWithTag("SceneManager");
        ShootingSceneManager shootingSceneManager = sceneManager.GetComponent<ShootingSceneManager>();
        shootingSceneManager.enemyDied();
    }
}
