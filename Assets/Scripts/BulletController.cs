﻿using UnityEngine;
using System.Collections;
using System;

/**
 * Controller for a bulet
 */
public class BulletController : MonoBehaviour
{

    public Vector3 target;
    public float speed = 50f;
    public float range = 50f;
    private float distanceTraveled = 0f;


    // Update is called once per frame
    void Update()
    {
        float distance = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, distance);

        distanceTraveled += distance;
        if (distanceTraveled >= range)
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        // destroy bullet on impact
        // damage will be calculated by the other object
        Destroy(gameObject);
    }

    /*
     * Change the trajectory of the bullet, so it flies through the target point for 
     * the distance equal to it's maximal range.
     */
    public void recalculateTarget()
    {
        Ray ray = new Ray(transform.position, target - transform.position);
        target = ray.GetPoint(range);
    }
}
